# findSO

Find .so files called by all SUID binaries that are either writable or don't exist (.so injection with constructor). Also look for service calls 'such as service apach2 start' (service hijacking via path or replacement).

### ToDo
- Add arg for single binary directive (no search and trace every possible suid)
- Change some checks to regex for better matching of format strings/service calls