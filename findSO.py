#!/usr/bin/python

import subprocess


RED     = "\033[1;31m"  
BLUE    = "\033[1;34m"
CYAN    = "\033[1;36m"
GREEN   = "\033[0;32m"
RESET   = "\033[0;0m"
BOLD    = "\033[;1m"
REVERSE = "\033[;7m"


called_libs = {}

def addin( lib, exe ):
    try:
        called_libs[ lib ].add( exe )
    except KeyError:
        called_libs[ lib ] = set()
        called_libs[ lib ].add( exe )


# All SUID binaries accessible
scanner = "find / -type f -perm -04000 -ls 2>/dev/null | rev | cut -d' ' -f1 | rev"
execs = subprocess.check_output( scanner, shell=True ).split()

for exe in execs:
    # Trace the execution of each binary
    print CYAN + "Tracing "+ exe + RESET

   
    try:
        tracer = "timeout 5 strace "+ exe +" 2>&1"
        traces = subprocess.check_output( tracer, shell=True).split()
    except:
        pass #print RED + "Unable to trace "+ exe + RESET

    for trace in traces:
        # If theres a .so file mentioned
        if ".so" in trace: 
            for _ in trace.split("\""):
                if "/" in _:
                    # Note it
                    addin( _, exe )
    
    try:
        strings = subprocess.check_output("strings "+exe+" 2>/dev/null", shell=True).split()
        # change this to regex match for better accuracy
        # look for '%s' possible format strings
        svs = 0;
        fms = 0;
        for string in strings:
            if "service" in string:
                svs += 1
            if "%s" in string and len(string) > 4:
                fms += 1
        if fms > 0: 
            print GREEN + "\t" + str(fms) + " possible format strings" + RESET
        if svs > 0: 
            print GREEN + "\t" + str(fms) +" possible service calls" + RESET
    except:
        pass

print CYAN + "\nDone.\n" + RESET

import os

print "="*10 + " Shared Libraries (writable or missing) " + "="*10 + "\n\n"

for lib, exs in called_libs.items():
    perm = subprocess.check_output("ls -l "+lib+" 2>/dev/null | cut -d' ' -f1", shell=True)
    if os.path.isfile( lib ):
        #Exists
        if os.access(lib, os.W_OK):
            #Writable
            print CYAN + lib + " is writable and is called by;"
            for e in exs: print GREEN + "\t" + e
            print RESET
    else:
        print CYAN + lib + " doesn't exist and is called by;"
        for e in exs: print GREEN + "\t" + e
        print RESET
